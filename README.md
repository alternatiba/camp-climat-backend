[![pipeline status](https://framagit.org/alternatiba/camp-climat-backend/badges/master/pipeline.svg)](https://framagit.org/alternatiba/camp-climat-backend/commits/master)

# Camp Climat Backend

Un template de REST API avec uniquement la gestion des utilisateurs.

La sécurité est entièrement gérée via des jetons JWT.
L'autentification est uniquement faisable par email/mot de passe.
Les processsus de validation et d'oublie des mots de passes y sont intégrés.

## La stack

- `docker` et `docker-compose` pour les conteneurs
- `nodejs` et `express` pour le serveur web
- `mongodb`pour la bdd (dans le docker-compose.yml uniquement pour les tests, MongoDB doit être installé sur un serveur à part)
- `typescript` parce que c'est génial, compilé avec `gulp`

## Run
### Option npm
cp .env.example .env
modifier les valeurs dans le .env
- HELLOASSO_*
- MONGODB_*
- SMTP_CONFIG
npm run build && npm test && npm start
