import * as passport from "passport";
import * as passportJwt from "passport-jwt";
import * as _ from "lodash";

import { default as User, UserModel } from "../entities/users";
import { Request, Response, NextFunction } from "express";

const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

let jwtParams = {
    secretOrKey: process.env.JWT_SECRET,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
    passReqToCallback: true
};

let cookieExtractor = (req: Request) => {
  var token = null;
  if (req && req.cookies)
  {
      token = req.cookies['JWT'];
  }
  return token;
};

let jwtCookieParams = {
  secretOrKey: process.env.JWT_SECRET,
  jwtFromRequest: cookieExtractor,
  passReqToCallback: true
};

/**
 * Sign in using JWT token
 */
passport.use('user-jwt', new JwtStrategy(jwtParams, (req: Request, payload: any, done: any) => {
  User.findById(payload.id, (err, user: UserModel) => {
    if (err) { return done(err); }
    if (!user) {
      return done(undefined, false, { message: `User ${payload.id} not found.` });
    }
    return done(null, user);
  });
}));

/**
 * Sign in using JWT token in cookies
 */
passport.use('user-jwt-cookie', new JwtStrategy(jwtCookieParams, (req: Request, payload: any, done: any) => {
  User.findById(payload.id, (err, user: UserModel) => {
    if (err) { return done(err); }
    if (!user) {
      return done(undefined, false, { message: `User ${payload.id} not found.` });
    }
    if (user.xsrfToken != req.headers["x-xsrf-token"]) {
      return done(undefined, false, { message: `X-XSRF-TOKEN header not found.` });
    }
    return done(null, user);
  });
}));

export let isUserSetBySelf = passport.authenticate(['user-jwt', 'user-jwt-cookie'], { session: false });

export let isAdmin = [
  isUserSetBySelf,
  (req: Request, res: Response, next: NextFunction) => {
    if (req.isAuthenticated() && req.user && req.user.isAdmin) {
      return next();
    } else {
      res.status(403).json({ error: "Not authorized." });
    }
  },
];

