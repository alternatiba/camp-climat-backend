import { logger } from '../services/logger';

const i18n = require('i18n-node-yaml')({
  debug: process.env.NODE_ENV !== 'production',
  translationFolder: __dirname + '/../../translations',
  locales: ['en_US', 'fr_FR'],
  defaultLocale: 'fr_FR',
  queryParameters: ['locale'],
});

i18n.ready.catch((error: any) => {
  logger.error('Failed loading translations', error);
});

let i18nMiddleware = i18n.middleware;

export { i18nMiddleware }