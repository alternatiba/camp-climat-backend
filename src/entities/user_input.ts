
import * as mongoose from "mongoose";

export interface UserInputModel extends mongoose.Document {

  ticketId: string;

  firstName?: string;
  lastName?: string;

  start?: Date;
  end?: Date;

  isAdmin: boolean;

  tel?: string;
  origin?: string;

  HelloAsso?: {
    email: string;
  }

  Coordo?: {
    email: string;
  }

  Fake?: {
    email: string;
  }

  email?: string;

  createdAt: Date;
  updatedAt: Date;

  getEmail: () => string;
  toPublic: () => Object;
  toAdmin: () => Object;
}

const UserInputSchema =  new mongoose.Schema({

  ticketId: String,

  firstName: String,
  lastName: String,

  tel: String,
  origin: String,

  start: Date,
  end: Date,

  isAdmin: {type: Boolean, default: false},

  HelloAsso: Object,
  Coordo: Object,
  Fake: Object,

  email: String,

}, { timestamps: true, collection: 'user_input' })

UserInputSchema.methods = {
  getEmail(): string {
    const user_input: UserInputModel = this;
    if (user_input.email) {
      return user_input.email;
    }
    if (user_input.Coordo) {
      return user_input.Coordo.email;
    }
    if (user_input.Fake) {
      return user_input.Fake.email;
    }
    if (user_input.HelloAsso) {
      return user_input.HelloAsso.email;
    }
    return '';
  },
  toPublic(): Object {
    const user_input: UserInputModel = this;
    return {
      id: user_input.id,
      ticketId: user_input.ticketId,
      email: user_input.getEmail()
    }
  },
  toAdmin(): Object {
    const user_input: UserInputModel = this;
    return Object.assign(user_input.toPublic(), {
      createdAt: user_input.createdAt,
      updatedAt: user_input.updatedAt
    })
  }
}

const UserInput = mongoose.model<UserInputModel>("UserInput", UserInputSchema);
export default UserInput;
