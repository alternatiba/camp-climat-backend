
import * as mongoose from "mongoose";
import { logger } from "../services/logger";

export interface CurriculumModel extends mongoose.Document {
  key: string;
  desc: string;
}

const CurriculumSchema =  new mongoose.Schema({
  key: {type: String, required: false},
  desc: {type: String, required: false},
}, { timestamps: true })

// CurriculumSchema.methods = {
//
// }


const Curriculum = mongoose.model<CurriculumModel>("Curriculum", CurriculumSchema);
export default Curriculum;
