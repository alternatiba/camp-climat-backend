
import * as mongoose from "mongoose";
import { logger } from "../services/logger";
import User from "./users";

interface TrainerModel {
  name: string;
  ticketId?: string;
}



export interface SessionModel extends mongoose.Document {

  start: Date;
  end: Date;
  length: number;

  trainers: [TrainerModel];

  title: string;

  room: string;
  descRoom : string;

  description: string;
  important: string;

  keyTraining: string;
  keySlot: string;

  domain: string;
  descDomain : string;
  assetDomain : string;
  colorDomain: string;
  curriculums:[Object];

  volume: number;
  busyVolume: number;

  dependsOn: string[];

  isInscriptionFree: boolean;
  isSecret: boolean;

  volunteerFactor?: number;

  createdAt: Date;
  updatedAt: Date;

  getOthersMulti: () => Promise<SessionModel[]>;
  conflictsWith: (otherSession: SessionModel) => boolean;
  toPublic: () => Object;
  toAdmin: () => Object;
}

const SessionSchema =  new mongoose.Schema({

  start: {type: Date, required: false},
  end: {type: Date, required: false},
  length: Number,

  trainers: [Object],
  title: {type: String, required: false},

  room: {type: String, required: false},
  descRoom : String,

  description: {type: String, required: false},
  important: {type: String, required: false},

  keyTraining: String,
  keySlot: String,

  domain: String,
  colorDomain:  String,
  descDomain : String,
  assetDomain : String,
  curriculums: [Object],

  volume: {type: Number, required: false, default: 1},
  busyVolume: {type: Number, required: false, default: 0},

  dependsOn: [String],

  isInscriptionFree:{type: Boolean, default: false},
  isSecret: {type: Boolean, default: false},

  volunteerFactor: Number,
}, { timestamps: true })

SessionSchema.pre('save', function save(next) {
  const session: any = this;
  return User.count({ sessions: session.keySlot })
  .then(result => {
    session.busyVolume = result;
    return next();
  })
})
//IMPORTANT this
SessionSchema.post('init', async function (session:SessionModel) {
  // let busy = await User.count({ sessions: session.keySlot });
  // session.busyVolume=busy;
    // Transform doc as needed here.  "this" is also the doc.
});

SessionSchema.methods = {
  getOthersMulti(): Promise<SessionModel[]> {
    const session: SessionModel = this;
    let result = session.keySlot.match(/(.*\[\d+\])\[\d+\]/);
    if (!result || result.length !== 2) {
      return Promise.resolve([session]);
    }
    let regex = new RegExp('^'+result[1].replace('[', '\\[').replace(']', '\\]'));
    return Session.find({keySlot: { $regex: regex }})
    .then(sessions => sessions);
  },
  conflictsWith(otherSession: SessionModel) {
    const session: SessionModel = this;
    return ((otherSession.isInscriptionFree!=true && session.isInscriptionFree!=true) && !(otherSession.end <= session.start || session.end <= otherSession.start)); // sessions overlap
  },
  toPublic(): Object {
    const session: SessionModel = this;
    return {
      id: session.id,
      start: session.start,
      end: session.end,
      length: session.length,
      trainers: session.trainers,
      title: session.title,
      room: session.room,
      descRoom: session.descRoom,
      description: session.description,
      important: session.important,
      keyTraining: session.keyTraining,
      keySlot: session.keySlot,
      domain: session.domain,
      descDomain : session.descDomain,
      assetDomain : session.assetDomain,
      colorDomain: session.colorDomain,
      curriculums: session.curriculums,
      volume: session.volume,
      busyVolume: session.busyVolume,
      dependsOn: session.dependsOn,
      volunteerFactor: session.volunteerFactor,
      isSecret: session.isSecret,
      isInscriptionFree: session.isInscriptionFree,
      isVolunteer: (session.volunteerFactor ? true : false),
      isFull : session.busyVolume >= session.volume
    }
  },
  toAdmin(): Object {
    const session: SessionModel = this;
    return Object.assign(session.toPublic(), {
      createdAt: session.createdAt,
      updatedAt: session.updatedAt
    })
  }
}


const Session = mongoose.model<SessionModel>("Session", SessionSchema);
export default Session;
