import * as bcrypt from 'bcrypt';
import * as uuid from 'uuid';
import * as crypto from 'crypto';
import * as mongoose from "mongoose";
import Session, { SessionModel } from './sessions';
import moment = require('moment');
import { logger } from '../services/logger';

export interface UserModel extends mongoose.Document {
  email: string;
  locale: string;

  isAdmin: boolean;

  password: string;
  passwordResetToken: string;
  passwordResetExpires: Date;

  xsrfToken: string;

  deletedAt: Date;

  ticketId: string; // from helloasso API (https://dev.helloasso.com/v3/responses#actions)

  arrivalDate: Date;
  leavingDate: Date;
  paid: boolean;

  comment: string;

  sessions: string[];

  firstname: string;
  lastname: string;
  tel: string;
  origin: string;
  wantPrint: boolean;
  curriculums: string[];

  createdAt: Date;
  updatedAt: Date;



  savePassword: (password: string) => Promise<UserModel>;
  comparePassword: (candidatePassword: string) => Promise<UserModel>;
  canRegister: (session: SessionModel) => Promise<boolean>;
  canRegisterFull: (session: SessionModel, isAdmin: Boolean) => Promise<CanRegisterResponse>;
  canUnregisterFull: (session: SessionModel, isAdmin: Boolean) => Promise<CanRegisterResponse>;
  toPublic: () => Object;
  toAdmin: () => Object;
}

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    index: { unique: true, sparse: true },
    required: false
  },
  locale: String,

  isAdmin: Boolean,

  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date,

  xsrfToken: { type: String, required: true, default: function() { return uuid.v4(); } },

  deletedAt: Date,

  ticketId: String,

  arrivalDate: Date,
  leavingDate: Date,
  paid: { type: Boolean, default: false },

  comment: String,

  sessions: [String],

  firstname: String,
  lastname: String,
  tel: String,
  origin: String,
  wantPrint: { type: Boolean, default: false },
  curriculums: { type: [String], default: [] }

}, { timestamps: true });

UserSchema.pre('save', async function(next) {
  const user: any = this;
  if (user.isNew && user.password) {
    user.email = user.email.toLowerCase();
    await user.savePassword(user.password);
    // return user.savePassword(user.password)
    //   .then(() => next());
  }
  next();
});

interface CanRegisterResponse {
  canRegister: Boolean;
  reason?: string;
  affectedSessions?: SessionModel[];

}


UserSchema.methods = {
  savePassword: function(newPassword: string): Promise<UserModel> {
    const user: UserModel = this;
    return new Promise<UserModel>((resolve, reject) => {
      bcrypt.genSalt(10, (err: any, salt: string) => {
        if (err) { return reject(err) }
        bcrypt.hash(newPassword, salt, (err: any, hash: string) => {
          if (err) { return reject(err) }
          user.password = hash;
          resolve(user);
        });
      });
    });
  },
  comparePassword: function(candidatePassword: string): Promise<UserModel> {
    const user: UserModel = this;
    return new Promise<UserModel>((resolve, reject) => {
      bcrypt.compare(candidatePassword, user.password, (err: any, isMatch: boolean) => {
        if (!err && isMatch) {
          resolve(user);
        } else {
          reject(err);
        }
      });
    });
  },
  canUnregisterFull: function(session: SessionModel, isAdmin: Boolean): Promise<CanRegisterResponse> {
    return new Promise(async (resolve, reject) => {
      if (isAdmin === false && session.domain === 'BENEVOL' && this.paid === true) {
        return resolve({ canRegister: false, reason: 'benevolPaid' });
      } else {
        return resolve({ canRegister: true});
      }
    });
  },
  canRegisterFull: function(session: SessionModel, isAdmin: Boolean): Promise<CanRegisterResponse> {
    return new Promise(async (resolve, reject) => {
      //try to register ever regitered
      if (this.sessions.includes(session.keySlot)) {
        return resolve({ canRegister: true });
      }

      try {
        session = await session.save();
      }
      catch (err) {
        return resolve({ canRegister: false, reason: 'unknown' });
      }
      //try to register busyVolume
      if (session.busyVolume >= session.volume) {
        return resolve({ canRegister: false, reason: 'busyVolume' });
      }

      //try to register out of Date
      let multi = await session.getOthersMulti();
      // console.warn('multi',multi);
      let multiOutOfDate = multi.filter(sess => {
        return sess.start < this.arrivalDate || sess.end > this.leavingDate
      })
      if (multiOutOfDate.length > 0 == true) {
        return resolve({ canRegister: false, reason: 'someSessionOutOfDate', affectedSessions: multiOutOfDate });
      }

      //tryToregister confict
      let mySessions = await Session.find({ "keySlot": { "$in": this.sessions } });
      let conflicts: SessionModel[] = [];
      let multiInConflict = multi.filter(sess => {
        conflicts = conflicts.concat(mySessions.filter(ms => { return sess.conflictsWith(ms) }));
      })
      if (conflicts.length > 0) {
        // console.warn('conflicts',conflicts);
        return resolve({ canRegister: false, reason: 'someSessionInConflict', affectedSessions: conflicts });
      }
      return resolve({ canRegister: true });

    });
  },
  toPublic(): Object {
    const user: UserModel = this;
    return {
      id: user.id,
      ticketId: user.ticketId,
      email: user.email,
      locale: user.locale,
      arrivalDate: user.arrivalDate,
      leavingDate: user.leavingDate,
      paid: user.paid,
      firstname: user.firstname,
      lastname: user.lastname,
      tel: user.tel,
      origin: user.origin,
      wantPrint: user.wantPrint,
      curriculums: user.curriculums,
      comment: user.comment,
      sessions: user.sessions,
      isAdmin: (user.isAdmin ? true : undefined),
    }
  },
  toAdmin(): Object {
    const user: UserModel = this;
    return Object.assign(user.toPublic(), {
      createdAt: user.createdAt,
      updatedAt: user.updatedAt
    })
  }
}

const User = mongoose.model<UserModel>("User", UserSchema);
export default User;
