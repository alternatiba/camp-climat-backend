import * as express from 'express';
import * as passport from 'passport';
import * as cors from 'cors';
import * as multer from 'multer';

import { logger, loggerMiddleware } from './services/logger';
import { UserRouter } from './controllers/user';
import { AuthRouter } from './controllers/auth';
import { SessionRouter } from './controllers/session';
import { HelloAssoRouter } from './controllers/helloasso';
import { CurriculumRouter } from './controllers/curriculum';
import { PdfRouter } from './controllers/pdf';

import { i18nMiddleware } from './config/i18n';

import mongoose = require('mongoose');
import expressValidator = require("express-validator");
import bodyParser = require('body-parser');
import cookieParser = require('cookie-parser');

declare module "mongoose" {
  export interface SchemaOptions {
    usePushEach?: boolean;
  }
}

declare global {
  namespace Express {
    export interface Request {
      apiMajorVersion: number;
      apiMinorVersion: number;
    }
  }
}

// Creates and configures an ExpressJS web server.
class App {

  // ref to Express instance
  public express: any;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.express.disable('x-powered-by');
    this.database();
    this.middleware();
    this.routes();
  }

  private database(): void {
    let mongoIp = process.env.MONGODB_IP || "";
    let mongoPort = process.env.MONGODB_PORT || "27017";
    let mongoUser = encodeURIComponent(process.env.MONGODB_USER || "");
    let mongoPasswd = encodeURIComponent(process.env.MONGODB_PASSWD || "");
    let mongoDb = process.env.MONGODB_DB || "";
    if (process.env.NODE_ENV == 'test') {
      mongoDb = mongoDb + '-test';
      // mongoUser = mongoUser + '-test';
    }
    let uri;
    if (process.env.MONGODB_URI) {
      // uri = process.env.MONGODB_URI;
      uri = `mongodb+srv://${mongoUser}:${mongoPasswd}@${mongoIp}/${mongoDb}?retryWrites=true`
    } else {
      uri = `mongodb://${mongoUser}:${mongoPasswd}@${mongoIp}:${mongoPort}/${mongoDb}?authSource=admin`;
    }
    mongoose.connect(uri, { useNewUrlParser: true })
    .catch(error => {
      logger.error("MongoDB connection error. Please make sure MongoDB is running.", error);
      process.exit();
    });
  }

  // Configure Express middleware.
  private middleware(): void {
    // if (process.env.NODE_ENV !== 'test') {
    this.express.use(loggerMiddleware);
    // }
    this.express.use(cors({credentials: true, origin: true}));
    // this.express.use(cors());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(cookieParser());
    this.express.use(expressValidator());
    this.express.use(passport.initialize());
    this.express.use(i18nMiddleware);
  }

  private versionMiddleware(req: express.Request, res: express.Response, next: express.NextFunction) {
    // let allowedVersions = ["v1.0", "v1.1"];
    let allowedVersions = ["v1.0"];

    if (req.params.version) {
      if (allowedVersions.indexOf(req.params.version) == -1) {
        return res.status(404).send();
      }
      let result = req.params.version.match(/v(\d)\.(\d+)/);
      if (result && result.length >= 2) {
        req.apiMajorVersion = result[0];
        req.apiMinorVersion = result[1];
        return next();
      }
      else {
        return res.status(404).send();
      }
    }
    else {
      req.apiMajorVersion = 1;
      req.apiMinorVersion = 0;
      return next();
    }
  }

  // Configure API endpoints.
  private routes(): void {
    let router = express.Router();
    // placeholder route handler
    router.use('/users', UserRouter);
    router.use('/sessions', SessionRouter);
    router.use('/auth', AuthRouter);
    router.use('/helloasso', HelloAssoRouter);
    router.use('/curriculums', CurriculumRouter);
    router.use('/pdf', PdfRouter);

    this.express.use('/:version(v\\d.\\d+)', this.versionMiddleware, express.static('documentation'));
    this.express.use('/:version(v\\d.\\d+)', this.versionMiddleware, router);

    this.express.use('/', express.static('documentation'));
    this.express.use('/', this.versionMiddleware, router);

    this.express.get('*', (req: any, res: any) => {
      res.status(404).send();
    });
  }

}

export default (new App()).express;
