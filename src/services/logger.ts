import * as util from 'util';
import * as _ from 'lodash';
import { transports, format, createLogger } from 'winston';
const { combine, timestamp, metadata, printf, colorize } = format;

import expressWinston = require('express-winston');

const consoleOpts: transports.ConsoleTransportOptions = {
  level: 'debug',
};

//Console
const consoleTransport = new (transports.Console)(consoleOpts);

const enabledTransports = [];

if (process.env.NODE_ENV == 'test') {
  consoleTransport.silent = true;
}
enabledTransports.push(consoleTransport);


const logFormat = printf((info) => {
  const { timestamp, ...meta } = info.metadata;
  // console.log(info)
  return `[${timestamp}] ${info.level}: ${info.message} ${_.isEmpty(meta) ? '' : util.inspect(meta, {breakLength: Infinity, depth: 3})}`;
});

const logger = createLogger({
  format: combine(
    timestamp({ format: 'DD/MM/YYYY HH:mm:ss' }),
    metadata(),
    colorize(),
    logFormat
  ),
  transports: enabledTransports
});

const loggerMiddleware = expressWinston.logger({
  winstonInstance: logger,
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: true,
});

logger.on('error', function (err: any) {
  console.log("logging error", err);
});

logger.info('backend server started!');

export { logger, loggerMiddleware };
