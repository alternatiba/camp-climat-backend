import * as request from 'request';
import * as moment from "moment";
import { logger } from './logger';

const auth_user = process.env.HELLOASSO_USER || '';
const auth_password = process.env.HELLOASSO_PASSWORD || '';
const campain = process.env.HELLOASSO_CAMPAIGN || '';

const base_url = `https://api.helloasso.com/v3`;
const payments_url = `${base_url}/campaigns/${campain}/payments.json?results_per_page=1000`;


export interface HelloAsso {

  email: string;

  ticketId: string;

  paidAt: Date;

  firstname: string;
  lastname: string;

  arrivalDate?: Date;
  leavingDate?: Date;

  tel?: string;
  origin?: string;
}

class HelloAssoApi {

  constructor() { }

  isPreRegistered(email: string) {
    return new Promise<boolean>((resolve, reject) => {
      request.get(payments_url, {
        auth: {
            user: auth_user,
            password: auth_password,
            sendImmediately: true
        },
        json: true
      }, (err: any, response: any, body: any) => {
        if (err) {
            return reject({request_error: err});
        }
        if (response.statusCode !== 200) {
          return reject({statusCode: response.statusCode});
        }
        const payments = body.resources;
        if (!payments) {
          return reject("no body.resources");
        }
        payments.forEach((payment: any) => {
          if (payment.payer_email === email) {
            return resolve(true);
          }
        });
        return resolve(false);
      })
    })
  }

  getHelloAssoFromAction(actionId: string) {
    return new Promise<HelloAsso>((resolve, reject) => {
      request.get(`${base_url}/actions/${actionId}.json`, {
        auth: {
            user: auth_user,
            password: auth_password,
            sendImmediately: true
        },
        json: true
      }, (err: any, response: any, body: any) => {
        if (err) {
          return resolve(null);
          // return reject({request_error: err});
        }
        if (response.statusCode !== 200) {
          return resolve(null);
          // return reject({statusCode: response.statusCode});
        }

        let helloassoInfo: HelloAsso = {
          email: body.email,
          ticketId: actionId,
          paidAt: body.date,
          firstname: body.first_name,
          lastname: body.last_name,
        };

        const infos = body.custom_infos;
        if (!infos) {
          return reject("no body.custom_infos");
        }
        infos.forEach((info: any) => {
          if (info.label === "Email") {
            helloassoInfo.email = info.value;
            return;
          }
          if (info.label === "Ton jour d'arrivée prévu ") {
            helloassoInfo.arrivalDate = moment.utc(info.value, 'DD/MM/YYYY').utcOffset(2).toDate();
            return;
          }
          if (info.label === "Ton jour de départ prévu ") {
            helloassoInfo.leavingDate = moment.utc(info.value, 'DD/MM/YYYY').utcOffset(2).toDate();
            return;
          }
          if (info.label === "Numéro de téléphone") {
            helloassoInfo.tel = info.value;
            return;
          }
          if (info.label === "Ville") {
            helloassoInfo.origin = info.value;
            return;
          }
        });
        return resolve(helloassoInfo);
      })
    })
  }

}

export default new HelloAssoApi;