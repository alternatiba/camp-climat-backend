import { Request, Response } from 'express';
import { createTransport } from 'nodemailer';
import { logger } from './logger';

let md = require('markdown').markdown;

console.log(process.env.SMTP_CONFIG || '');
let transporter = createTransport(process.env.SMTP_CONFIG || '');

class Mailer {

  constructor() { }

  sendMail (req: Request, res: Response, emailKey: string, emailInfos: any): Promise<void> {
    return new Promise((resolve, reject) => {
      if (process.env.NODE_ENV === 'test') {
        return resolve();
      }
      let emailParams = Object.assign({
        from: 'Camp Climat <campclimat@alternatiba.eu>',
        to: req.user.email,
        bcc: 'campclimat@alternatiba.eu',
        subject: res.locals.t(`emails.${emailKey}.subject`, emailInfos),
        html: md.toHTML(res.locals.t(`emails.${emailKey}.body`, emailInfos) || ''),
        text: res.locals.t(`emails.${emailKey}.body`, emailInfos),
      }, emailInfos);

      if (process.env.EMAIL_INTERCEPTOR) {
        emailParams = Object.assign(emailParams, {
          to: process.env.EMAIL_INTERCEPTOR,
          subject: `[DEV] To:${emailParams.to} - ${emailParams.subject}`,
        });
      }

      transporter.sendMail(emailParams, (error: any) => {
        if (error) {
          logger.error('Send mail failed', error);
          reject(error);
        } else {
          logger.info('Email sent with params', emailParams);
          resolve();
        }
      });
    });
  }
}

export default new Mailer();