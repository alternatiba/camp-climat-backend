import * as dotenv from 'dotenv';
dotenv.config();

import * as chai from 'chai';
import chaiHttp = require('chai-http');

import * as jwt from 'jsonwebtoken';

import { default as app } from "../app";

import User from '../entities/users';
import Session from '../entities/sessions';

import { HelloAsso, default as HelloAssoApi } from "../services/helloasso";
import UserInput from '../entities/user_input';

chai.use(chaiHttp);
const expect = chai.expect;

describe('API Tests !', () => {

  let adminId: string;

  let user1Id: string;
  let user2Id: string;
  let deletedUserId: string;

  let resetToken: string;

  let helloassoUser1: HelloAsso = {
    ticketId: "F001",
    email: "campclimat-application@alternatiba.eu",
    paidAt: new Date(),
    firstname: "Saturnin",
    lastname: "Mesnil",
    arrivalDate: new Date("2019-07-31T00:00:00.000+02:00"),
    leavingDate: new Date("2019-08-11T00:00:00.000+02:00")
  };
  let helloassoUserInput1 = new UserInput({
    ticketId: "F001",
    email: "campclimat-application@alternatiba.eu",
    isAdmin: false,
    Fake: null,
    firstname: "Saturnin",
    lastname: "Mesnil",
    start: new Date("2019-07-31T00:00:00.000+02:00"),
    end: new Date("2019-08-11T00:00:00.000+02:00"),
  });
  let helloassoUser2: HelloAsso = {
    email: "jonasmarchon88@gmail.com",
    ticketId: "000073917202",
    paidAt: new Date(),
    firstname: "Jonas",
    lastname: "Marchon"
  };
  let admin = new User({
    firstname: 'Admin',
    lastname: 'TESTER',
    email: 'admin@toto.com',
    password: 'admin',
    locale: 'fr_FR',
    arrivalDate: new Date("2019-07-31T00:00:00.000Z"),
    leavingDate: new Date("2019-08-11T00:00:00.000Z"),
    isAdmin: true
  });
  let user1 = new User({
    firstname: 'User1',
    lastname: 'TESTER',
    email: 'user1@toto.com',
    password: 'user1',
    locale: 'fr_FR',
    arrivalDate: new Date("2019-07-31T00:00:00.000Z"),
    leavingDate: new Date("2019-08-11T00:00:00.000Z"),
  });
  let deletedUser = new User({
    firstname: 'User2',
    lastname: 'TESTER',
    email: 'user2@toto.com',
    password: 'user2',
    locale: 'fr_FR',
    arrivalDate: new Date("2019-07-31T00:00:00.000Z"),
    leavingDate: new Date("2019-08-11T00:00:00.000Z"),
  });
  let session1 = new Session({
    location: "Amphi 4",
    start: new Date("2019-08-01T11:00:00+02:00"),
    end: new Date("2019-08-01T12:00:00+02:00"),
    trainers: ["Simon"],
    title: "Comment développer une application pour le camp climat",
    abstract: "bla bla ...",
    dependsOn: [],
    keySlot: 'TEST[20][1]',
    volume: 30,
    busyVolume: 0
  });
  let session2Full = new Session({
    location: "Amphi 3",
    start: new Date("2019-08-02T11:00:00+02:00"),
    end: new Date("2019-08-02T12:00:00+02:00"),
    trainers: ["Alex"],
    title: "Comment animer une réunion",
    abstract: "bla bla ...",
    dependsOn: [],
    keySlot: 'TEST[21][1]',
    volume: 0,
    busyVolume: 0
  });
  let session3Depends = new Session({
    location: "Amphi 3",
    start: new Date("2019-08-03T11:00:00+02:00"),
    end: new Date("2019-08-03T12:00:00+02:00"),
    trainers: ["Alex"],
    title: "Comment animer une réunion",
    abstract: "bla bla ...",
    dependsOn: [],
    keySlot: 'TEST[22][1]',
    volume: 10,
    busyVolume: 0
  });

  before(done => {
    Promise.all([
      User.deleteMany({}),
      Session.deleteMany({})
    ])
    .then(() => admin.save())
    .then(admin => {
      adminId = admin.id;
    })
    .then(() => helloassoUserInput1.save())
    .then(() => user1.save())
    .then(user => {
      user1Id = user.id;
    })
    .then(() => deletedUser.save())
    .then(user => { deletedUserId = user.id })
    .then(() => session1.save())
    .then(() => session2Full.save())
    .then(() => session3Depends.save())
    .then(() => done());
  });

  after(done => {
    Promise.all([
      User.deleteMany({}),
      Session.deleteMany({})
    ])
    .then(() => done());
  });

  describe('Users', () => {

    describe('POST /users', () => {

      it('should not create a new user if email is not provided', () => {
        let user = {
          ticketId: "000080762442",
          password: 'test',
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(400);
        })
      });

      it('should not create a new user if password is not provided', () => {
        let user = {
          ticketId: "000080762442",
          email: 'test@toto.com',
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(400);
        })
      });

      it('should not create a new user if ticketId is not provided', () => {
        let user = {
          email: 'test@toto.com',
          password: 'test',
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(400);
        })
      });

      it('should not create a new user if password is too short', () => {
        let user = {
          ticketId: "000080762442",
          email: 'test@toto.com',
          password: 'tot'
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(400);
        })
      });

      it('should not create a new user if email is badly formatted', () => {
        let user = {
          ticketId: "000080762442",
          email: 'user1@toto',
          password: 'test',
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(400);
        })
      });

      it('should not create a new user if email allready exists', () => {
        let user = {
          ticketId: "000080762442",
          email: user1.email,
          password: 'test',
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(409);
        })
      });

      it('should not create a new user if helloasso user not found', () => {
       let user = {
         ticketId: "009080762442",
         email: 'test@toto.com',
        password: 'test',
       }
       return chai.request(app)
       .post('/users')
       .send(user)
       .then(res => {
        expect(res).to.have.status(403);
        })
      });

      it('should create a new user', () => {
        let user = {
          ticketId: helloassoUser1.ticketId,
          email: helloassoUser1.email,
          password: 'test',
        }
        return chai.request(app)
        .post('/users')
        .send(user)
        .then(res => {
          expect(res).to.have.status(201);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'arrivalDate', 'leavingDate'
          ]);
          user2Id = res.body.id;
          expect(res.body.arrivalDate).to.be.eq(helloassoUser1.arrivalDate.toISOString());
          expect(res.body.leavingDate).to.be.eq(helloassoUser1.leavingDate.toISOString());
        })
      });

    });

    describe('GET /users/me', () => {

      it('should give the user details', () => {
        let payload = { id: user2Id };
        let token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/me')
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'arrivalDate', 'leavingDate'
          ]);
        })
      });

      it('should not give the user details if not admin', () => {
        let payload = { id: user2Id };
        let token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/' + user1Id)
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(403);
        })
      });

      it('should give the user details if admin', () => {
        let payload = { id: adminId };
        let token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/' + user1Id)
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'arrivalDate', 'leavingDate'
          ]);
        })
      });

    });

    describe('PATCH /users/me', () => {

      it('should update password', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          password: 'test2'
        };
        return chai.request(app)
        .patch('/users/me')
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'arrivalDate', 'leavingDate'
          ]);
        })
      });

      it('should update account informations', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          firstname: 'Test2',
          lastname: 'TEST2',
          locale: 'fr_FR',
          arrivalDate: new Date("2019-08-01T00:00:00.000Z"),
          leavingDate: new Date("2019-08-08T00:00:00.000Z"),
        };
        return chai.request(app)
        .patch('/users/me')
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'firstname', 'lastname', 'locale',
            'arrivalDate', 'leavingDate'
          ]);
          expect(res.body.firstname).to.be.eq(body.firstname);
          expect(res.body.lastname).to.be.eq(body.lastname);
          expect(res.body.locale).to.be.eq(body.locale);
          expect(res.body.arrivalDate).to.be.eq(body.arrivalDate.toISOString());
          expect(res.body.leavingDate).to.be.eq(body.leavingDate.toISOString());
        })
      });

    });

    describe('GET /users', () => {

      it('should be forbidden for non-admin users', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users')
        .then(res => {
          expect(res).to.have.status(401);
        })
      })

      it('should list all users', () => {
        const payload = { id: adminId };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users')
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.an('array');
          expect(res.body).to.have.length(4);
        })
      })

    });

    describe('GET /users/search?q=query&s=start&l=limit', () => {

      it('should be forbidden for non-admin users', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/search')
        .then(res => {
          expect(res).to.have.status(401);
        })
      })

      it('should be list all users with query "admin"', () => {
        const payload = { id: adminId };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/search?q=admin')
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.an('array');
          expect(res.body).to.have.length(1);
        })
      })

      it('should be list all users with query "toto" with limit "2"', () => {
        const payload = { id: adminId };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/search?q=toto&l=2')
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.an('array');
          expect(res.body).to.have.length(2);
        })
      })

    });

    describe('GET /users/stats', () => {

      it('should be forbidden for non-admin users', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/stats')
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(403);
        })
      })

      it('should display some stats', () => {
        const payload = { id: adminId };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .get('/users/stats')
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.contain.keys([
            'users'
          ]);
        })
      })

    });

    describe('DELETE /users/:id', () => {

      it('should be forbidden for non-admin users if desired user id different from self', () => {
        const payload = { id: user1Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .del('/users/' + user2Id)
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(403);
        })
      })

      it('should mark the specified user as "deleted" if user is admin', () => {
        const payload = { id: adminId };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .del('/users/' + deletedUserId)
        .set('Authorization', 'JWT ' + token)
        .then(res => {
          expect(res).to.have.status(200);
          return User.findById(deletedUserId);
        })
        .then(user => {
          expect(user.deletedAt).to.be.a('Date');
        })
      })

    });

  });

  describe('User Sessions', () => {

    describe('POST /users/:id/register', () => {

      it('should not add the specified sessionKey to the user sessions if the session is full', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          session: session2Full.keySlot
        };
        return chai.request(app)
        .post(`/users/me/register`)
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(409);
        })
      });

      it('should add the specified sessionKey to the user sessions', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          session: session1.keySlot
        };
        return chai.request(app)
        .post(`/users/me/register`)
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(201);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'sessions'
          ]);
          expect(res.body.sessions).to.include(session1.keySlot);
          return Session.findOne({keySlot: session1.keySlot});
        })
        .then(session => {
          expect(session.busyVolume).to.eq(session1.busyVolume + 1);
        })
      });

      it('should not increment busySlots if the user is allready registered', () => {
        const payload = { id: user2Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          session: session1.keySlot
        };
        return chai.request(app)
        .post(`/users/me/register`)
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(201);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'sessions'
          ]);
          expect(res.body.sessions).to.include(session1.keySlot);
          return Session.findOne({keySlot: session1.keySlot});
        })
        .then(session => {
          expect(session.busyVolume).to.eq(session1.busyVolume + 1);
        })
      });

      it('should register to the session if admin', () => {
        const payload = { id: adminId };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          session: session1.keySlot,
        };
        return chai.request(app)
        .post(`/users/${user1Id}/register`)
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(201);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'sessions'
          ]);
          expect(res.body.sessions).to.include(session1.keySlot);
          return Session.findOne({keySlot: session1.keySlot});
        })
        .then(session => {
          expect(session.busyVolume).to.eq(session1.busyVolume + 2);
        })
      });

    });

    describe('POST /users/:id/unregister', () => {

      it('should unsubscribe from a session', () => {
        const payload = { id: user1Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        const body = {
          session: session1.keySlot,
        };
        return chai.request(app)
        .post(`/users/me/unregister`)
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.contain.keys([
            'id', 'email',
            'sessions'
          ]);
          expect(res.body.sessions).to.not.include(session1.keySlot);
          return Session.findOne({keySlot: session1.keySlot});
        })
        .then(session => {
          expect(session.busyVolume).to.eq(session1.busyVolume + 1);
        })
      });

    });

  })

  describe('Auths', () => {

    describe('POST /auth/password', () => {

      it('should return a valid JWT access token if credentials are ok', () => {
        const body = {
          email: 'user1@toto.com',
          password: 'user1',
        };
        return chai.request(app)
        .post('/auth/password')
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.keys([
            'jwt_token'
          ]);
        })
      });

      it('should return valid JWT and XSRF token if "cookie" is true', () => {
        const body = {
          email: 'user1@toto.com',
          password: 'user1',
          cookie: true
        };
        return chai.request(app)
        .post('/auth/password')
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.keys([
            'jwt_token', 'xsrf_token'
          ]);
          expect(res).to.have.cookie('JWT');
        })
      });

      it('should throw an error if credentials are not ok', () => {
        const body = {
          email: 'user1@toto.com',
          password: 'Xuser1',
        };
        return chai.request(app)
        .post('/auth/password')
        .send(body)
        .then(res => {
          expect(res).to.have.status(401);
        })
      });

    });

    describe('POST /auth/password/forgot', () => {

      it('should return after setting "passwordResetToken" on the user', () => {
        const body = {
          email: 'user1@toto.com',
        };
        return chai.request(app)
        .post('/auth/password/forgot')
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          return User.findById(user1Id)
        })
        .then(user => {
          expect(user.passwordResetToken).to.be.a('string');
          resetToken = user.passwordResetToken;
        })
      });

      it('should throw an error if email doesn\'t exists', () => {
        const body = {
          email: 'user12@toto.com',
        };
        return chai.request(app)
        .post('/auth/password/forgot')
        .send(body)
        .then(res => {
          expect(res).to.have.status(404);
        })
      });

    });

    describe('POST /auth/password/reset?token=resetToken', () => {

      it('should return a success message if everything ok', () => {
        const body = {
          password: 'toto',
        };
        return chai.request(app)
        .post('/auth/password/reset?token=' + resetToken)
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
        })
      });

      it('should throw an error if token doesn\'t exists', () => {
        const body = {
          password: 'toto',
        };
        return chai.request(app)
        .post('/auth/password/reset?token=' + 'totototototo')
        .send(body)
        .then(res => {
          expect(res).to.have.status(404);
        })
      });

      it('should throw an error if password is not sent', () => {
        const body = {
        };
        return chai.request(app)
        .post('/auth/password/reset?token=' + resetToken)
        .send(body)
        .then(res => {
          expect(res).to.have.status(404);
        })
      });

    });

    describe('POST /auth/refresh', () => {

      it('should return a valid JWT if the one sent was correct', () => {
        const body = {
        };
        const payload = { id: user1Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .post('/auth/refresh')
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.keys([
            'jwt_token'
          ]);
        })
      });

      it('should return a valid JWT and XSRF if the jwt sent was correct', () => {
        const body = {
          cookie: true
        };
        const payload = { id: user1Id };
        const token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
        return chai.request(app)
        .post('/auth/refresh')
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.keys([
            'jwt_token', 'xsrf_token'
          ]);
          expect(res).to.have.cookie('JWT');
        })
      });

      it('should not renew JWT if the one sent was incorrect', () => {
        const body = {
        };
        const payload = { id: user1Id };
        const token = jwt.sign(payload, 'toto' || "", { expiresIn: "15 days" });
        return chai.request(app)
        .post('/auth/refresh')
        .set('Authorization', 'JWT ' + token)
        .send(body)
        .then(res => {
          expect(res).to.have.status(401);
        })
      });

    });

  });

});
