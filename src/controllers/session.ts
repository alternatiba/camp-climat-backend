import { Request, Response, NextFunction, Router } from 'express';

import { default as Session, SessionModel } from '../entities/sessions';
import { default as User, UserModel } from '../entities/users';

import { logger } from '../services/logger';
import { isUserSetBySelf, isAdmin } from '../config/passport';
import moment = require('moment');

export class SessionController {

  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  listAll (req: Request, res: Response) {
    const user: UserModel = req.user;

    const queryDay = req.query.day;
    const queryVolunteer = req.query.volunteer;
    let sessionQuery = Session.find({});

    if (queryDay) {
      let startDay = moment(queryDay);
      let endDay = moment(queryDay).add(1, 'd');
      sessionQuery = sessionQuery.where({start: {$gte: startDay.toDate(), $lt: endDay.toDate()}});
    }
    if (queryVolunteer) {
      if (queryVolunteer === '1') {
        sessionQuery = sessionQuery.where({domain: 'BENEVOL'});
      }
      if (queryVolunteer === '0') {
        sessionQuery = sessionQuery.where({domain: {$ne: 'BENEVOL'}});
      }
    }

    return sessionQuery
    .sort({ start: 1 })
    .then((sessions: SessionModel[]) => {
      let sessionsList = sessions.map(session => {
        // if(session.keySlot=="DET32[2][1]"){
        //   console.warn('Service',session.keySlot,session.busyVolume);
        // }
        const sessionData = user.isAdmin ? session.toAdmin() : session.toPublic();
        return Object.assign(sessionData, {
          registered: user.sessions.includes(session.keySlot)
        })
      })
      return res.status(200).json(sessionsList);
    })
    .catch(error => {
      if (!res.headersSent) {
        logger.error(error);
        res.status(500).send();
      }
    })
  }

  read (req: Request, res: Response) {
    const user: UserModel = req.user;
    const sessionKey: string = req.params.key;

    return Session.findOne({keySlot: sessionKey})
    .then((session: SessionModel) => {
      if (!session) {
        return res.status(404).send();
      }
      const sessionData = user.isAdmin ? session.toAdmin() : session.toPublic();
      return res.status(200).json(Object.assign(sessionData, {
        registered: user.sessions.includes(session.keySlot)
      }));
    })
    .catch(error => {
      if (!res.headersSent) {
        logger.error(error);
        res.status(500).send();
      }
    })
  }

  listUsers (req: Request, res: Response) {
    const user: UserModel = req.user;
    const sessionKey: string = req.params.key;
    return Session.findOne({keySlot: sessionKey})
    .then((session: SessionModel) => {
      if (!session) {
        return res.status(404).send();
      }
      console.warn('read user',user)
      if (!user.isAdmin && !session.trainers.some(trainer => trainer.ticketId === user.ticketId)) {
        return res.status(403).send();
      }
      return User.find({ sessions: sessionKey })
      .then((users: UserModel[]) => {
        return res.status(200).json(users.map(u => (user.isAdmin ? u.toAdmin() : u.toPublic())));
      })
      .catch(error => {
        if (!res.headersSent) {
          logger.error(error);
          res.status(500).send();
        }
      })
    })
  }

  setupRoutes () {
    this.router.get('/', isUserSetBySelf, this.listAll);
    this.router.get('/:key(*)/users', isUserSetBySelf, this.listUsers);
    this.router.get('/:key(*)', isUserSetBySelf, this.read);
  }
}

let sessionController = new SessionController();
const SessionRouter = sessionController.router;

export { SessionRouter };
