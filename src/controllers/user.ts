import { Request, Response, NextFunction, Router } from 'express';

import { default as User, UserModel } from '../entities/users';
import { default as Session, SessionModel } from '../entities/sessions';
import UserInput from '../entities/user_input';

import { logger } from '../services/logger';
import { default as HelloAssoApi, HelloAsso } from '../services/helloasso';
import { isUserSetBySelf, isAdmin } from '../config/passport';

import * as _ from "lodash";

declare global {
  namespace Express {
    export interface Request {
      requestedUser: UserModel;
    }
  }
}

export class UserController {

  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  create(req: Request, res: Response) {
    req.checkBody("email", "Email is not valid").isEmail();
    req.checkBody("ticketId", "ticketId is not valid").isAlphanumeric();
    req.checkBody("password", "Password must be at least 4 characters long").isLength({ min: 4, max: undefined });
    req.sanitize("email").normalizeEmail({
      gmail_remove_dots: false,
      gmail_remove_subaddress: false,
      outlookdotcom_remove_subaddress: false,
      yahoo_remove_subaddress: false,
      icloud_remove_subaddress: false,
    });

    const errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }

    let ticketId: string = req.body.ticketId;

    let user = new User({
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      ticketId: ticketId,
      email: req.body.email.toLowerCase(),
      password: req.body.password,
      locale: (req.body.locale || 'fr_FR')
    });

    logger.info("New user trying to singup", { controller: "User", email: user.email, locale: user.locale });

    return User.findOne({
      $or: [
        { 'email': user.email },
        { 'ticketId': user.ticketId },
      ]
})
      .then((existingUser: UserModel | null) => {
        if (existingUser) {
          if (existingUser.email == user.email) {
            logger.warn("Email allready exists, sending 409", { email: user.email, ticketId: user.ticketId });
            res.status(409).send('email already exists');
          } else if (existingUser.ticketId == user.ticketId) {
            logger.warn("TicketId allready exists, sending 499", { email: user.email, ticketId: user.ticketId });
            res.status(499).send('ticketId already exists');
          }
          return Promise.reject("existing user");
        }
        return UserInput.findOne({ ticketId: ticketId })
          .then(userInput => {
            // logger.warn("userInput",userInput)
            if (!userInput) {
              res.status(403).send('user not pre-registered');
              // if (ticketId.length < 12) {
              //   ticketId = ticketId.padStart(23, '0');
              // }
              // return HelloAssoApi.getHelloAssoFromAction(ticketId)
              // .then((helloasso) => {
              //   if (!helloasso) {
              //     logger.warn("User was not pre-registered on helloasso, sending 404", { email: user.email, ticketId: user.ticketId });
              //     res.status(403).send('user not pre-registered');
              //     return Promise.reject("not pre-registered");
              //   }
              //   if (!user.firstname) {
              //     user.firstname = helloasso.firstname;
              //   }
              //   if (!user.lastname) {
              //     user.lastname = helloasso.lastname;
              //   }
              //   user.ticketId = helloasso.ticketId;
              //   user.arrivalDate = helloasso.arrivalDate || new Date("2019-07-31T00:00:00.000Z");
              //   user.leavingDate = helloasso.leavingDate || new Date("2019-08-11T00:00:00.000Z");
              //   user.tel = helloasso.tel;
              //   user.origin = helloasso.origin;
              //
              //   // user.generateEmailToken()
              //   return user.save()
              //   .then((savedUser) => {
              //     user = savedUser;
              //     logger.info("Password - Creating new User", { email: user.email });
              //     return res.status(201).json(savedUser.toPublic());
              //   })
              // });
            } else {
              if (!user.firstname) {
                user.firstname = userInput.firstName;
              }
              if (!user.lastname) {
                user.lastname = userInput.lastName;
              }
              user.ticketId = userInput.ticketId;
              user.arrivalDate = userInput.start || new Date("2019-07-31T00:00:00.000+02:00");
              user.leavingDate = userInput.end || new Date("2019-08-11T00:00:00.000+02:00");
              user.tel = userInput.tel;
              user.isAdmin = userInput.isAdmin;
              user.origin = userInput.origin;

              // user.generateEmailToken()
              return user.save()
                .then((savedUser) => {
                  user = savedUser;
                  logger.info("Password - Creating new User", { email: user.email });
                  return res.status(201).json(savedUser.toPublic());
                })
            }
          })
      })
      .catch((error: any) => {
        if (!res.headersSent) {
          logger.error(error);
          res.status(500).json({ error: 'Server error' });
        }
      });
  }

  read(req: Request, res: Response) {
    const user: UserModel = req.requestedUser;
    return res.status(200).json(req.user.isAdmin ? user.toAdmin() : user.toPublic());
  }

  update(req: Request, res: Response) {

    const user: UserModel = req.requestedUser;

    req.checkBody("email", "Email is not valid").optional().isEmail();
    req.checkBody("password", "Password must be at least 4 characters long").optional().isLength({ min: 4, max: undefined });
    req.sanitize("email").normalizeEmail({
      gmail_remove_dots: false,
      gmail_remove_subaddress: false,
      outlookdotcom_remove_subaddress: false,
      yahoo_remove_subaddress: false,
      icloud_remove_subaddress: false,
    });

    const errors = req.validationErrors();

    if (errors) {
      logger.warn("update request errors", errors);
      return res.status(400).send(errors);
    }

    // updating password, ignoring other stuff.
    if (req.body.password) {
      logger.info("User updating password", { email: user.email });
      return user.savePassword(req.body.password)
        .then((user) => user.save())
        .then((user) => {
          logger.info("User updated", { email: user.email });
          return res.status(200).json(req.user.isAdmin ? user.toAdmin() : user.toPublic());
        }).catch((err) => {
          logger.error("User update error", err);
          return res.status(400).json(req.user.isAdmin ? user.toAdmin() : user.toPublic());
        })
    }

    let allowedUpdates = ['firstname', 'lastname', 'comment', 'locale', 'arrivalDate', 'leavingDate','paid', 'tel', 'origin', 'wantPrint', 'curriculums'];
    // updating other stuff
    Object.keys(req.body).forEach(key => {
      if (allowedUpdates.indexOf(key) === -1 && req.user.isAdmin !== true) {
        return;
      }
      if (user[key] != req.body[key] && (Array.isArray(req.body[key]) || req.body[key] !== "")) {
        logger.info("User updating key", { email: user.email, key: key, oldValue: user[key], newValue: req.body[key] });
        user[key] = req.body[key];
      }
    });
    if (req.body['arrivalDate'] || req.body['leavingDate']) {
      let updatedSessions: string[] = [];
      return Promise.all(user.sessions.map(sessionKey => {
        return Session.findOne({ keySlot: sessionKey })
          .then(session => {
            if (!session) {
              return;
            }
            if (session.start < user.arrivalDate || session.end > user.leavingDate) {
              return session.getOthersMulti()
                .then(multiSessions => {
                  return Promise.all(multiSessions.map(sess => {
                    let sessKey = sess.keySlot;
                    const index = user.sessions.indexOf(sessKey);
                    if (index > -1) {
                      user.sessions.splice(index, 1);
                      // sess.busyVolume = sess.busyVolume - 1;
                    }
                    if (!updatedSessions.includes(sessKey)) {
                      updatedSessions.push(sessKey);
                    }
                    return user.save()
                    .then(() => sess.save())
                  }));
                })
            }
          })
      }))
        .then(() => user.save())
        .then((updatedUser) => {
          let returnUser = req.user.isAdmin ? updatedUser.toAdmin() : updatedUser.toPublic();
          Object.assign(returnUser, { updatedSessions: updatedSessions });
          return res.status(200).json(returnUser);
        })
        .catch((err) => {
          logger.error("User update error", err);
          return res.status(500).send();
        })
    }
    return user.save()
      .then((user) => {
        logger.info("User updated", { email: user.email });
        return res.status(200).json(req.user.isAdmin ? user.toAdmin() : user.toPublic());
      })
      .catch((err) => {
        logger.error("User update error", err);
        return res.status(400).json(req.user.isAdmin ? user.toAdmin() : user.toPublic());
      })
  }

  listAll(req: Request, res: Response) {
    User.find({ deletedAt: { $exists: false } }, (err, users) => {
      if (err) { return res.status(500).send(); }
      if (!users) { return res.status(404).send(); }
      return res.status(200).json(
        _.map(users, (user) => req.user.isAdmin ? user.toAdmin() : user.toPublic())
      );
    });
  }

  search(req: Request, res: Response) {
    req.checkQuery("q").optional().escape();
    req.checkQuery("s").optional().isNumeric();
    req.checkQuery("l").optional().isNumeric();
    // req.checkQuery("o").optional().isIn(["asc", "desc"]);

    const errors = req.validationErrors();

    if (errors) {
      logger.warn("query errors", errors);
      return res.status(400).send();
    }

    let query = req.query["q"] || ".";
    let start = Number(req.query["s"] || 0);
    let limit = Number(req.query["l"] || 50);
    User.find(
      {
        $and: [
          { deletedAt: { $exists: false } },
          {
            $or: [
              { email: RegExp(query, 'i') },
              { firstname: RegExp(query, 'i') },
              { lastname: RegExp(query, 'i') },
              { ticketId: RegExp(query, 'i') },
            ]
          }
        ]
      }
    )
      .sort({ "_id": 1 })
      .skip(start).limit(limit)
      .then(users => {
        if (!users) { return res.status(404).send(); }
        return res.status(200).json(
          users.map((user) => req.user.isAdmin ? user.toAdmin() : user.toPublic())
        );
      }).catch((err) => {
        logger.error("User search error", err);
        return res.status(500).send();
      })
  }

  stats(req: Request, res: Response) {
    let stats = {
      users: 0,
    }

    return Promise.all([
      User.find({}).countDocuments({}).then(nbUsers => {
        stats.users = nbUsers;
      })
    ])
      .then(() => {
        return res.status(200).json(stats);
      })
      .catch((err) => {
        logger.error("User stats error", err);
        return res.status(500).send();
      });
  }

  delete(req: Request, res: Response) {
    const user: UserModel = req.requestedUser;
    user.deletedAt = new Date();
    user.email = user.email + '_deleted_' + user.deletedAt.valueOf();
    user.save()
      .then(() => {
        return res.status(200).send();
      })
      .catch((err) => {
        logger.error("User delete error", err);
        return res.status(500).send();
      });
  }

  async register(req: Request, res: Response) {
    req.checkBody("session", "missing 'session' body parameter (the session key)");

    const errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }
    const user: UserModel = req.requestedUser;
    const userConnected: UserModel = req.user;
    const sessionKey: string = req.body.session;

    try{
      let session= await Session.findOne({ keySlot: sessionKey });
      if (!session) {
         res.status(404).send();
      }else{
        let canRegister = await user.canRegisterFull(session,userConnected.isAdmin);
        if (!canRegister.canRegister) {
          res.status(409).json(canRegister);
        }else{
          let multiSessions = await session.getOthersMulti();
          for (let sess of multiSessions){
            let sessKey = sess.keySlot;
            if (!user.sessions.includes(sessKey)) {
              user.sessions.push(sessKey);
              // sess.busyVolume = sess.busyVolume + 1;
            }
            await user.save();
            await sess.save();
          }
          let returnUser = req.user.isAdmin ? user.toAdmin() : user.toPublic();
          Object.assign(returnUser, { updatedSessions: multiSessions.map(s => s.keySlot) });
          return res.status(201).json(returnUser);
        }
      }
    }catch(e){
      if (!res.headersSent) {
        logger.error(e);
        res.status(500).send();
      }
    }
  }

  async unregister(req: Request, res: Response) {
    req.checkBody("session", "missing 'session' body parameter (the session key)");

    const errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }
    try{
      const user: UserModel = req.requestedUser;
      const userConnected: UserModel = req.user;
      const sessionKey: string = req.body.session;

      let session = await Session.findOne({ keySlot: sessionKey });
      if (!session) {
        res.status(404).send();
      }else{
        let canUnregister = await user.canUnregisterFull(session,userConnected.isAdmin);
        if (!canUnregister.canRegister) {
          res.status(409).json(canUnregister);
        }else{
          let multiSessions= await session.getOthersMulti();
          let counter=1;
          for (let sess of multiSessions){
            let sessKey = sess.keySlot;
            const index = user.sessions.indexOf(sessKey);
            if (index > -1) {
              user.sessions.splice(index, 1);
            }
            await user.save();
            await sess.save();
          }
          let returnUser = req.user.isAdmin ? user.toAdmin() : user.toPublic();
          Object.assign(returnUser, { updatedSessions: multiSessions.map(s => s.keySlot) });
          res.status(200).json(returnUser);
        }
      }
    }catch(e){
      if (!res.headersSent) {
        logger.error(e);
        res.status(500).send();
      }
    }


    //
    //
    // Session.findOne({ keySlot: sessionKey })
    //   .then(session => {
    //     if (!session) {
    //       return res.status(404).send();
    //     }
    //
    //
    //     return session.getOthersMulti()
    //       .then(multiSessions => {
    //         return Promise.all(multiSessions.map(sess => {
    //           let sessKey = sess.keySlot;
    //           const index = user.sessions.indexOf(sessKey);
    //           if (index > -1) {
    //             user.sessions.splice(index, 1);
    //             // sess.busyVolume = sess.busyVolume - 1;
    //           }
    //           return user.save()
    //           .then(() => sess.save())
    //         }))
    //         .then(() => user.save())
    //         .then((updatedUser) => {
    //           let returnUser = req.user.isAdmin ? updatedUser.toAdmin() : updatedUser.toPublic();
    //           Object.assign(returnUser, { updatedSessions: multiSessions.map(s => s.keySlot) });
    //           return res.status(200).json(returnUser);
    //         })
    //     });
    //   })
    //   .catch(error => {
    //     if (!res.headersSent) {
    //       logger.error(error);
    //       res.status(500).send();
    //     }
    //   })

  }

  requestedUserMiddleware(req: Request, res: Response, next: NextFunction) {
    const user: any = req.user;

    if (req.params.id == "me" || req.params.id == user.id) {
      req.requestedUser = user;
      return next();
    }
    else {
      if (user.isAdmin) {
        User.findById(req.params.id, (err, requestedUser) => {
          if (err) { return res.status(500).send(err); }
          if (!requestedUser) { return res.status(404).send(); }
          req.requestedUser = requestedUser;
          return next();
        })
      }
      else {
        return res.status(403).send();
      }
    }
  }

  setupRoutes() {
    this.router.post('/', this.create);
    this.router.get('/', isAdmin, this.listAll);
    this.router.get('/search', isAdmin, this.search);
    this.router.get('/stats', isAdmin, this.stats);
    this.router.get('/:id', isUserSetBySelf, this.requestedUserMiddleware, this.read);
    this.router.patch('/:id', isUserSetBySelf, this.requestedUserMiddleware, this.update);
    this.router.delete('/:id', isUserSetBySelf, this.requestedUserMiddleware, this.delete);

    this.router.post('/:id/register', isUserSetBySelf, this.requestedUserMiddleware, this.register);
    this.router.post('/:id/unregister', isUserSetBySelf, this.requestedUserMiddleware, this.unregister);
  }
}

let userController = new UserController();
const UserRouter = userController.router;

export { UserRouter };
