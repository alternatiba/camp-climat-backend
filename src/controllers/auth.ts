import { Request, Response, NextFunction, Router } from 'express';
import { default as User, UserModel } from '../entities/users';
import * as jwt from 'jsonwebtoken';
import { logger } from '../services/logger';
import { default as Mailer } from '../services/mailer';
import { isUserSetBySelf } from '../config/passport';
import * as crypto from 'crypto';

export class AuthController {

  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  passwordToken (req: Request, res: Response) {
    req.checkBody("email", "Email is empty or not valid")
      .notEmpty().isEmail();
    req.checkBody("password", "Password is empty")
      .notEmpty();
    req.checkBody("cookie", "cookie should be a boolean")
      .isBoolean().optional();

    req.sanitize("email").normalizeEmail({
      gmail_remove_dots: false,
      gmail_remove_subaddress: false,
      outlookdotcom_remove_subaddress: false,
      yahoo_remove_subaddress: false,
      icloud_remove_subaddress:false,
    });

    if (req.body.cookie) {
      req.body.cookie = (req.body.cookie === "true" || req.body.cookie === true)
    }

    const errors = req.validationErrors();

    if (errors) {
      logger.warn("Auth password 400 error", errors);
      return res.status(400).send(errors);
    }

    logger.verbose("Trying to get token with email/password");

    User.findOne({ email: req.body.email, password: { $exists: true } })
    .then((user: UserModel | null) => {
      if (!user) {
        // res.status(404).json({ error: 'No user found with this email' });
        return Promise.reject('No user found with this email')
      }
      if (user.deletedAt) {
        res.status(410).send();
        return Promise.reject('User is deleted');
      }
      return user.comparePassword(req.body.password);
    })
    .then((user: UserModel) => {
      let payload = { id: user.id };
      let token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
      let jsonResponse: Object = { jwt_token : token };
      if (req.body.cookie) {
        res.cookie('JWT', token, {
          httpOnly: true,
          secure: (process.env.NODE_ENV == 'production'),
          expires: new Date(Date.now() + 15 * 24 * 60 * 60000 )
        });
        jsonResponse["xsrf_token"] = user.xsrfToken;
      }
      res.status(200).json(jsonResponse);
    })
    .catch((error: any) => {
      if (!res.headersSent) {
        logger.error(error);
        res.status(401).json({ error: 'Incorrect email or password' });
      }
    })
  }

  passwordForgot (req: Request, res: Response) {
    req.checkBody("email", "Email is empty or not valid").notEmpty().isEmail();
    req.sanitize("email").normalizeEmail({
      gmail_remove_dots: false,
      gmail_remove_subaddress: false,
      outlookdotcom_remove_subaddress: false,
      yahoo_remove_subaddress: false,
      icloud_remove_subaddress:false,
    });

    const errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }

    return new Promise<string>((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        const token = buf.toString('hex');
        if (err) reject(err);
        else resolve(token);
      });
    })
    .then((token: string) => {
      return User.findOne({ email: req.body.email })
      .then((user: UserModel | null) => {
        if (!user) {
          res.status(404).json({ error: 'No user found with this email' });
          return Promise.reject('No user found with this email')
        }
        if (user.deletedAt) {
          res.status(410).send();
          return Promise.reject('User is deleted');
        }
        user.passwordResetToken = token;
        user.passwordResetExpires = new Date(Date.now() + 3600000); // 1 hour
        return user.save();
      })
    })
    .then((user: UserModel) => {
      req.user = user;
      return Mailer.sendMail(req, res, 'send_forgot_password', {
        to: user.email,
        link: process.env.CAMP_CLIMAT_RESET_PASSWD_URL + user.passwordResetToken
      })
    })
    .then(() => {
      res.status(200).json({ message : "email sent" });
    })
    .catch((error: any) => {
      if (!res.headersSent) {
        logger.error(error);
        res.status(500).json({ error: 'Server error' });
      }
    })
  }

  passwordReset (req: Request, res: Response) {
    req.checkBody("password", "Password must be at least 4 characters long").optional().isLength({min:4, max: undefined});
    // req.checkBody("confirmPassword", "Passwords do not match").optional().equals(req.body.password);
    req.check("token").notEmpty();

    const errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }

    const token = req.query.token || req.body.token || '';

    User.findOne({ passwordResetToken: token })
      .where('passwordResetExpires').gt(Date.now())
      .then((user: UserModel | null) => {
        if (!user) {
          res.status(404).json({ error: 'Invalid reset token' });
          return Promise.reject('Invalid reset token')
        }
        if (user.deletedAt) {
          res.status(410).send();
          return Promise.reject('User is deleted');
        }
        return user.savePassword(req.body.password);
      })
      .then((user: UserModel) => {
        user.passwordResetToken = '';
        return user.save();
      })
      .then((user: UserModel) => {
        req.user = user;
        return Mailer.sendMail(req, res, 'send_reset_password', {
          to: user.email,
          email: user.email
        })
      })
      .then(() => {
        res.status(200).json({ message : "password changed" });
      })
      .catch((error: any) => {
        if (!res.headersSent) {
          logger.error(error);
          res.status(500).json({ error: 'Server error' });
        }
      })
  }

  refresh (req: Request, res: Response) {
    if (req.user.deletedAt) {
      res.status(410).send();
      return Promise.reject('User is deleted');
    }
    let payload = { id: req.user.id };
    let token = jwt.sign(payload, process.env.JWT_SECRET || "", { expiresIn: "15 days" });
    let jsonResponse: Object = { jwt_token : token };
    if (req.body.cookie) {
      res.cookie('JWT', token, {
        httpOnly: true,
        secure: true,
        expires: new Date(Date.now() + 15 * 24 * 60 * 60000 )
      });
      jsonResponse["xsrf_token"] = req.user.xsrfToken;
    }
    res.status(200).json(jsonResponse);
  }

  setupRoutes () {
    this.router.post('/password', this.passwordToken);
    this.router.post('/password/forgot', this.passwordForgot);
    this.router.post('/password/reset', this.passwordReset);
    this.router.post('/refresh', isUserSetBySelf, this.refresh);
  }
}

let authController = new AuthController();
authController.setupRoutes();
const AuthRouter = authController.router;

export { AuthRouter };
