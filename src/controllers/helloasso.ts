import { Request, Response, NextFunction, Router } from 'express';

import { logger } from '../services/logger';
import { default as UserInput, UserInputModel } from '../entities/user_input';

export class HelloAssoController {

  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  preregister(req: Request, res: Response) {

    logger.debug("helloasso notification", req.body);

    return res.status(200).send();
  }

  info(req: Request, res: Response) {
    const ticketId = req.params.id;
    UserInput.findOne({ticketId: ticketId})
    .then((userInput: UserInputModel) => {
      if (!userInput) {
        return res.status(404).send();
      }
      return res.status(200).json(userInput.toPublic());
    })
    .catch(error => {
      if (!res.headersSent) {
        logger.error(error);
        res.status(500).send();
      }
    })
  }

  setupRoutes () {
    this.router.get('/info/:id', this.info);
    this.router.post('/preregister', this.preregister);
  }
}

let helloAssoController = new HelloAssoController();
const HelloAssoRouter = helloAssoController.router;

export { HelloAssoRouter };