import { Request, Response, NextFunction, Router } from 'express';
import { default as Curriculum, CurriculumModel } from '../entities/curriculums';
import { logger } from '../services/logger';
import { isUserSetBySelf, isAdmin } from '../config/passport';
import moment = require('moment');

export class CurriculumController {

  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  listAll(req: Request, res: Response) {
    let curriculumQuery = Curriculum.find({});
    return curriculumQuery
      .then((curriculums: CurriculumModel[]) => {
        return res.status(200).json(curriculums);
      })
      .catch(error => {
        if (!res.headersSent) {
          logger.error(error);
          res.status(500).send();
        }
      })
  }


  setupRoutes() {
    this.router.get('/', isUserSetBySelf, this.listAll);
  }
}

let curriculumController = new CurriculumController();
const CurriculumRouter = curriculumController.router;

export { CurriculumRouter };
