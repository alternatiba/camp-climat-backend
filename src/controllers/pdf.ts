import { Request, Response, NextFunction, Router } from 'express';
import { default as Session, SessionModel } from '../entities/sessions';
import { default as User, UserModel } from '../entities/users';

import { logger } from '../services/logger';
import { isUserSetBySelf, isAdmin } from '../config/passport';
import moment = require('moment');
import momentTz = require('moment-timezone');
import PDFDocument = require('pdfkit');
import request = require('request');

const url_rooms: string = "https://grappe.io/data/api/5d30d48afccfdc00a0e41207-rooms"

// Fonction pour créer le planning d'un User
async function createPlanning(doc: any, currentUser: UserModel, newPage: boolean) {

  if (newPage)
    doc.addPage();

  doc.fontSize(30);
  doc.text("Mon planning pour le Camp Climat", {align: 'center'});
  doc.fontSize(20).text(currentUser.firstname + " " + currentUser.lastname, {
    align: 'center'
  });

  let sessions: Array<SessionModel> = [];

  // Pour chaque formation, ajouter les détails de la formation
  let sessionsQuery = Session.find({'keySlot': currentUser.sessions});
  let sessionsRes = await sessionsQuery.exec();

  for (let session of sessionsRes)
    if (session != null)
      sessions.push(session);

  // On regarde aussi les formations auxquels on est Formateur
  let queryTrainerSession = Session.find({trainers: { $elemMatch: {ticketId: currentUser.ticketId}}});
  let sessionsTrainer = await queryTrainerSession.exec();

  for (let session of sessionsTrainer)
    if (session != null)
      sessions.push(session);

  // On trie le tableau
  sessions.sort((a: SessionModel, b: SessionModel) => {
    if (moment(a.start).isBefore(moment(b.start))) { return -1; }
    else if (moment(a.start).isAfter(moment(b.start))) { return 1; }
    return 0;
  });

  // Pour chaque formation, on la met dans le PDF
  var currentDay: moment.Moment = null;
  //const offsetx: number = 40;
  for (let session of sessions) {
    // Ecrire le jour si pas encore mis
    if (currentDay == null || currentDay.isBefore(moment(session.start), 'day')) {
      currentDay = moment(session.start);
      doc.moveDown();
      doc.font('Helvetica-Bold').fontSize(14);
      doc.x = doc.x-40;
      doc.text(currentDay.locale('fr').format("dddd D MMMM"));
      doc.x = doc.x+40;
      doc.font('Helvetica').fontSize(10);
    }

    doc.moveDown()
    .text(momentTz.tz(session.start, 'Europe/Paris').format("HH:mm") + " - " +
          momentTz.tz(session.end, 'Europe/Paris').format("HH:mm") + " ",
          {continued: true})
    .font('Helvetica-Bold').text(session.title).font('Helvetica');
    if (session.assetDomain != null)
      doc.image('./src/assets/' + session.assetDomain.split(' ').join(''), doc.x-30, doc.y-10, {width: 30})

    doc.text(session.descRoom, {continued: true});

    let isTrainer: boolean = false;
    for (let trainer of session.trainers) {
      if (trainer.ticketId == currentUser.ticketId) {
        isTrainer = true;
        break;
      }
    }

    function getRefererLabel (session: SessionModel) {
      if (session.domain=="ENF"){
        return "Animateur.trice"
      } else if (session.domain=="DET"){
        return "Intervenant.e"
      } else if (session.domain=="BENEVOL"){
        return "Référent.e"
      } else {
        return "Formateur.trice"
      }
    }

    doc.text(isTrainer?getRefererLabel(session):"Participant.e", {align: 'right'});
    doc.text(session.important);
  }
}

// Fonction pour mettre en forme un tableau
function createTable(doc: any, rows: Array<any>, fontsize: number, textspacer: number) {
  doc.font('Helvetica', fontsize);

  const pageWidth = Math.round(doc.page.width - doc.page.margins.left - doc.page.margins.right);
  const pageHeight = Math.round(doc.page.height - doc.page.margins.top - doc.page.margins.bottom);
  const textSpacer = textspacer;

  let { y } = doc;
  const { x } = doc;

  rows.forEach(row => {
    // table border
    const arr = row.map((column: any) =>
      doc.heightOfString(column.text, { width: column.width * pageWidth })
    );

    const cellHeight = Math.max(...arr) + textSpacer * 2;
    doc.lineWidth(0.3);
    doc.strokeColor('lightgrey');

    doc
    .lineJoin('miter')
    .rect(x, y, pageWidth, cellHeight)
    .stroke();

    let writerPos = x;
    for (let i = 0; i < row.length - 1; i++) {
      writerPos += row[i].width * pageWidth;

      doc
      .lineCap('butt')
      .moveTo(writerPos + textSpacer, y)
      .lineTo(writerPos + textSpacer, y + cellHeight)
      .stroke();
    }

    // table text
    let textWriterPos = x;
    for (let i = 0; i < row.length; i++) {
      doc.text(row[i].text, textWriterPos + textSpacer, y + textSpacer, {
        continued: false,
        width: row[i].width * pageWidth - (textSpacer + 5),
      });
      textWriterPos += row[i].width * pageWidth + (textSpacer - 5);
    }

    y += cellHeight;

    if (pageHeight < y - cellHeight) {
      doc.addPage();
      y = 40;
    }
  });

  doc.moveDown(2);
  doc.text('', doc.page.margins.left);
}

// Fonction pour créer la page de description d'une session
function createSession(
  doc: any,
  session: SessionModel,
  users: Array<UserModel>,
  newPage: boolean) {

  if (newPage)
    doc.addPage();

  // Page de garde
  doc.fontSize(40).text(session.title, {align: 'center'}).fontSize(15)
  .text(moment(session.start).locale('fr').format("dddd D MMMM YYYY") +
        " de " + momentTz.tz(session.start, 'Europe/Paris').format("HH:mm") + " à " +
        momentTz.tz(session.end, 'Europe/Paris').format("HH:mm"), {align: 'center'})
  .text(" au " + session.descRoom + " (" + session.room + ")",
        {align: 'center'});

  doc.moveDown();
  doc.text(session.description);
  doc.moveDown();

  doc.text("Référent.e.s")

  session.trainers.forEach((trainer) => {
    doc.fontSize(12).text(trainer.name);
  });

  doc.moveDown()
  .fontSize(15).text('Participant.e.s');

  function newRow(firstname: string,
    lastname: string,
    email: string,
    tel: string,
    origin:string) {
      return [
        {
          text: firstname,
          width: 0.15,
        },
        {
          text: lastname,
          width: 0.15,
        },
        {
          text: email,
          width: 0.3,
        },
        {
          text: tel,
          width: 0.2,
        },
        {
          text: origin,
          width: 0.2
        }
      ]
  }

  let volunteerTable = []

  for (let user of users) {
    volunteerTable.push(newRow(
      user.firstname,
      user.lastname,
      user.email,
      user.tel,
      user.origin
    ));
  }

  createTable(doc, volunteerTable, 10, 8);
}

export class PDFController {

  router: Router;

  constructor() {
    this.router = Router();
    this.setupRoutes();
  }

  async roomsPDF(req: Request, res: Response) {
    var doc = new PDFDocument();

    // Fonction pour les lignes du tableau
    function newRow(title: string, date: string, duration: string) {
      return [
        {text: title,
         width: 0.6},
        {text: date,
         width: 0.2},
        {text: duration,
          width: 0.2}
      ]
    }

    return request(url_rooms, async (er, re, body) => {
      if (er) {
        logger.warn("Can't get rooms.");
        return res.status(500).send();
      }

      // List of rooms
      var rooms: Array<string> = [];

      body = JSON.parse(body);

      for (let room of body) {
        rooms.push(room.key);
      }

      // Request header
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/pdf');
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-disposition', 'attachment; filename=Untitled.pdf');

      // PDF in http response
      doc.pipe(res);
      doc.font('Helvetica');
      doc.fontSize(70);

      // Page de garde
      doc.text('Planning des formations par salle', {
        align: 'center',
      });
      doc.fontSize(20);

      for (let room of rooms) {
        // Si la sesssion existe bien
        let querySessions = Session.find({room: room, domain: {$ne: 'BENEVOL'}});
        querySessions = querySessions.sort({start: 1});

        let sessions = await querySessions.exec();

        // Si 0 session,
        if (sessions.length == 0) {
          continue;
        }

        // Titre
        doc.addPage()
        .fontSize(20).text('Créneaux formations pour ' + sessions[0].room, {
          align: 'center',
        });
        doc.fontSize(10).text(sessions[0].descRoom, {
          align: 'center',
        }).fontSize(12);

        let sessionsTable = []
        sessionsTable.push(newRow('Titre', 'Date', 'Horaire'));

        for (let session of sessions) {
          if (!session.title)
            continue;
          sessionsTable.push(newRow(
            session.title.replace(/\s*$/, ""),
            moment(session.start).locale('fr').format("dddd D MMMM"),
            momentTz.tz(session.start, 'Europe/Paris').format("HH:mm") + " à " +
            momentTz.tz(session.end, 'Europe/Paris').format("HH:mm")
          ));
        }
        createTable(doc, sessionsTable, 10, 10);
      }

      doc.end();
      return res.status(200).send();
    });
  }

  async volunteerPDF(req: Request, res: Response) {
    var doc = new PDFDocument();

    // Récupérer le jour
    const queryDay: string = req.params.date;

    // Si la date est mal formé, ou si elle est passé, erreur
    if (!moment(queryDay).isValid() ||
        moment(queryDay) < moment()) {
      logger.warn("Bad date for pdf generation.");
      doc.end();
      return res.status(500).send();
    }

    // Request header
    res.statusCode = 200;
    res.setHeader('Content-type', 'application/pdf');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-disposition', 'attachment; filename=Untitled.pdf');

    // PDF in http response
    doc.pipe(res);
    doc.font('Helvetica');
    doc.fontSize(70);

    // Page de garde
    doc.text('Créneaux bénévolats du ' + moment(queryDay).locale('fr').format("dddd D MMMM YYYY"), {
      align: 'center',
    });
    doc.fontSize(20);

    // Récupérer toutes les sessions bénévolats de ce jour
    let sessionQuery = Session.find({});
    let startDay = moment(queryDay);
    let endDay = moment(queryDay).add(1, 'd');
    sessionQuery = sessionQuery.where({start: {$gte: startDay.toDate(), $lt: endDay.toDate()}});
    sessionQuery = sessionQuery.where({domain: 'BENEVOL'});
    sessionQuery = sessionQuery.where({isSecret: 'false'})

    let sessions = await sessionQuery.exec();

    for (let session of sessions) {
      let usersQuery = User.find({ sessions: session.keySlot })
      let users = await usersQuery.exec();

      createSession(doc, session, users, true);
    }

    doc.end()
    return res.status(200);
  }

  async sessionPDF(req: Request, res: Response) {
    var doc = new PDFDocument();

    // Récupérer le jour
    const keySession: string = req.params.key;

    // Récupérer les infos de la session
    let sessionQuery = Session.findOne({keySlot: keySession});
    let session = await sessionQuery.exec();

    // Si pas de session
    if (!session) {
      return res.status(404).send();
    }

    // Récupérer les users
    let usersQuery = User.find({ sessions: session.keySlot })
    let users = await usersQuery.exec();

    // Request header
    res.statusCode = 200;
    res.setHeader('Content-type', 'application/pdf');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-disposition', 'attachment; filename=Untitled.pdf');

    // PDF in http response
    doc.pipe(res);
    doc.font('Helvetica');
    doc.fontSize(30);

    // Create session page
    createSession(doc, session, users, false);

    doc.end()
    return res.status(200);
  }

  async planningPDF(req: Request, res: Response) {
    var doc = new PDFDocument();
    var currentUser = req.requestedUser;

    // Request header
    res.statusCode = 200;
    res.setHeader('Content-type', 'application/pdf');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-disposition', 'attachment; filename=Untitled.pdf');

    // PDF in http response
    doc.pipe(res);
    doc.font('Helvetica');

    await createPlanning(doc, currentUser, false);

    doc.end()
    return res.status(200);
  }

  async referentsPDF(req: Request, res: Response) {
    var doc = new PDFDocument();

    // Tour de toutes les sessions pour trouver tous les trainers
    let querySessions = Session.find();
    var sessions = await querySessions.exec();

    // Récupérer tous les référents
    var referentsTicketId = new Set();
    for (let session of sessions) {
      for (let trainer of session.trainers) {
        if (trainer.ticketId != null)
          referentsTicketId.add(trainer.ticketId);
      }
    }

    // Récupérer les users avec un ticketId dans l'ensemble
    let queryReferents = User.find({ticketId: Array.from(referentsTicketId)});
    queryReferents = queryReferents.sort({lastname: 1});
    var referents = await queryReferents.exec();

    // Request header
    res.statusCode = 200;
    res.setHeader('Content-type', 'application/pdf');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-disposition', 'attachment; filename=Untitled.pdf');

    // PDF in http response
    doc.pipe(res);
    doc.font('Helvetica');

    var newPage = false;
    for (let referent of referents) {
      await createPlanning(doc, referent, newPage);
      newPage = true;
      const range = doc.bufferedPageRange();
      if ((range.start + range.count) % 2 == 1)
        doc.addPage();
    }

    doc.end()
    return res.status(200);
  }


  requestedUserMiddleware(req: Request, res: Response, next: NextFunction) {
    const user: any = req.user;

    if (req.params.id == "me" || req.params.id == user.id) {
      req.requestedUser = user;
      return next();
    }
    else {
      if (user.isAdmin) {
        User.findById(req.params.id, (err, requestedUser) => {
          if (err) { return res.status(500).send(err); }
          if (!requestedUser) { return res.status(404).send(); }
          req.requestedUser = requestedUser;
          return next();
        })
      }
      else {
        return res.status(403).send();
      }
    }
  }

  setupRoutes() {
    this.router.get('/session/:key(*)', isUserSetBySelf, this.sessionPDF);
    this.router.get('/planning/:id', isUserSetBySelf, this.requestedUserMiddleware, this.planningPDF);
    this.router.get('/volunteer/:date', isAdmin, this.volunteerPDF);
    this.router.get('/rooms', isAdmin, this.roomsPDF);
    this.router.get('/referents', isAdmin, this.referentsPDF);
  }
}

let pdfController = new PDFController();
const PdfRouter = pdfController.router;

export { PdfRouter };
