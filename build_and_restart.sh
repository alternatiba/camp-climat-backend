#!/bin/bash

docker-compose build --no-cache
docker-compose stop
docker-compose rm -f
docker-compose up -d
