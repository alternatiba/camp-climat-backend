FROM node:10-stretch

USER node

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

# RUN apt-get update
# RUN apt-get install -y locales 

# RUN locale-gen en_US.UTF-8
# ENV LANG en_US.UTF-8
# ENV LANGUAGE en_US:en
# ENV LC_ALL en_US.UTF-8

RUN mkdir -p /home/node/app && chown node:node -R /home/node/app && chmod 777 -R /home/node/app

WORKDIR /home/node/app

RUN npm install -g gulp-cli

COPY --chown=node:node package*.json ./

RUN npm install

