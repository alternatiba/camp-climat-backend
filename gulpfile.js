const gulp = require('gulp');
const ts = require('gulp-typescript');

// pull in the project TypeScript config
const tsProject = ts.createProject('tsconfig.json');

gulp.task("default", function () {
  return tsProject.src()
      .pipe(tsProject())
      .js.pipe(gulp.dest("build"));
});

gulp.task("build", function () {
  return tsProject.src()
      .pipe(tsProject())
      .js.pipe(gulp.dest("build"));
});
